const express = require("express");
const mongoose = require("mongoose");

const port = 3000;

const app = express();

app.use(express.json());

// connect to db
mongoose
  .connect(
    "mongodb+srv://jroda:admin@zuittbatch243-roda.4axrb9t.mongodb.net/?retryWrites=true&w=majority"
  )
  .then(() => {
    // listen for request
    app.listen(port, () => console.log(`Server running at port ${port}`));
  })
  .catch((err) => {
    console.log(err);
  });

const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: String,
  password: String,
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  const { username, password } = req.body;
  User.findOne({ username }, (err, result) => {
    if (result != null && result.username === username) {
      return res.send("Username already used.");
    } else {
      let newUser = new User({
        username,
        password,
      });

      newUser.save((err, savedTask) => {
        if (err !== null) {
          return console.log(err.message);
        } else {
          return res.status(201).send("New user registered");
        }
      });
    }
  });
});

app.get("/signup", (req, res) => {
  User.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});
